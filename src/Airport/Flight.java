package Airport;

/**
 *
 * @author rushil
 */
import java.util.Date;
public class Flight {

    /**
     * @param args the command line arguments
     */
    private String number;
    private Date date;
        
    public Flight(String number, Date date) {
        this.number = number;
        this.date = date;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public String getNumber() {
        return this.number;
    }
    
    public Date getDate() {
        return this.date;
    }
    
}
