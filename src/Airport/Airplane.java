package Airport;

/**
 *
 * @author rushil
 */
public class Airplane {
    private String make;
    private String model;
    private int id;
    private int seats;

    public Airplane(String make, String model, int id, int seats) {
        this.make = make;
        this.model = model;
        this.id = id;
        this.seats = seats;
    }
    
    public String getMake() {
        return this.make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }
    
}
