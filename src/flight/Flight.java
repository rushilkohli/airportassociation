/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flight;

/**
 *
 * @author rushil
 */
import java.util.Date;
public class Flight {

    /**
     * @param args the command line arguments
     */
    private String code;
    private Date date;
    
    public Flight(String code, Date date) {
        this.code = code;
        this.date = date;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public Date getDate() {
        return this.date;
    }
    
}
